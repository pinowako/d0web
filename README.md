# LHCb Masterclass Web Version
International Masterclasses is a program providing highschool student to discover particle physics.
The LHCb Masterclass application focuses on the visualization of events recorded by the LHCb detector and building of histograms.

This project is a web version of this projet https://gitlab.cern.ch/LHCbOutreach/LHCbMasterclass and was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Install Node
Install node and npm on your system, (alternatively download them from: https://nodejs.org/en/ https://www.npmjs.com/get-npm )

## Development server

To run it :

    git clone https://gitlab.cern.ch/LHCbOutreach/d0web.git
    cd d0web
    npm install
    ng serve

Then navigate to `http://localhost:4200/`
