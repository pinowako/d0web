/**
* (c) Copyright 2019 CERN
*
* This software is distributed under the terms of the GNU General Public
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".
*
* In applying this licence, CERN does not waive the privileges and immunities
* granted to it by virtue of its status as an Intergovernmental Organization
* or submit itself to any jurisdiction.
*/

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AboutComponent } from './about/about.component';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DatasetSelectComponent } from './dataset-select/dataset-select.component'
import { EventDisplayComponent } from './event-display/event-display.component';
import { LSAExerciseComponent } from './lsa-exercise/lsa-exercise.component';
import { EventService } from './services/event.service';
import { LSAdataService } from './services/lsadata.service';
import { AuthGuard } from './services/auth-guard.service';
import { CombinationService } from './services/combination.service';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatTabsModule} from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressButtonsModule } from 'mat-progress-buttons';
import { MatSliderModule } from '@angular/material/slider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NouisliderModule } from 'ng2-nouislider';
import { FooterComponent } from './footer/footer.component';
import { ModalComponent } from './modal/modal.component';
import { KatexModule } from 'ng-katex';


const appRoutes: Routes = [
  { path: 'about', component: AboutComponent},
  { path: 'home', component: HomeComponent},
  { path: 'dataset-select', component: DatasetSelectComponent},
  { path: 'event-display', canActivate: [AuthGuard], component: EventDisplayComponent},
  { path: 'lsa-exercise', component: LSAExerciseComponent},
  { path: '', component: HomeComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AboutComponent,
    HomeComponent,
    DatasetSelectComponent,
    EventDisplayComponent,
    LSAExerciseComponent,
    FooterComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MatSlideToggleModule,
    MatCardModule,
    MatSliderModule,
    MatSelectModule,
    MatButtonModule,
    MatDialogModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressButtonsModule,
    MatCheckboxModule,
    NouisliderModule,
    KatexModule
  ],
  providers: [
    EventService,
    LSAdataService,
    CombinationService,
    AuthGuard
  ],
  bootstrap: [
    AppComponent
  ],
  exports: [
    ModalComponent
  ],
  entryComponents: [
    ModalComponent
  ]
})
export class AppModule { }
