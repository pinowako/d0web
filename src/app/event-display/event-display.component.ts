/**
* (c) Copyright 2019 CERN                                                     
*                                                                             
* This software is distributed under the terms of the GNU General Public      
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   
*                                                                             
* In applying this licence, CERN does not waive the privileges and immunities 
* granted to it by virtue of its status as an Intergovernmental Organization  
* or submit itself to any jurisdiction.                                       
*/

import { AfterViewInit, AfterContentInit, OnInit, Component, ElementRef, Input, ViewChild, HostListener } from '@angular/core';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { VSDEvent, Track, EventService } from '../services/event.service';
import { Student, CombinationService } from '../services/combination.service'
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { ToastrService } from 'ngx-toastr' ;
import * as THREE from 'three';
import { CSG } from 'three-csg-ts';
import * as d3 from 'd3';
import transition from 'd3-transition';
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader'
import { SVGLoader, SVGResult } from 'three/examples/jsm/loaders/SVGLoader'
import { NouiFormatter } from 'ng2-nouislider';
import { MatSliderModule } from '@angular/material/slider';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { ModalComponent } from '../modal/modal.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ShapePath } from 'three';

@Component({
  selector: 'app-event-display',
  templateUrl: './event-display.component.html',
  styleUrls: ['./event-display.component.css']
})
export class EventDisplayComponent implements OnInit {
  title: string = "Visual Analysis";

  currentStudent = {} as Student;

  // scene properties
  private scene: THREE.Scene;
  private camera3d: THREE.PerspectiveCamera;
  private camerarphi: THREE.PerspectiveCamera;
  private camerarhoz: THREE.PerspectiveCamera;
  private renderer: THREE.WebGLRenderer;
  private controls: OrbitControls;
  private autorotate: boolean = false;

  // slider toggle property
  checkedDetector: boolean = true;
  checkedTracks: boolean = true;
  checkedCombos: boolean = false;
  checkedClusters: boolean = true;
  changetoggle: MatSlideToggleChange;
  particleSelected: any;
  private help: any;

  // picking track properties
  private INTERSECTED: any;
  private INTERSECTEDprev: any;
  private INTERSECTED2: any;

  private ED3D_width = 1/2;
  private ED3D_height = 1/2;

  private THREEDVP = new THREE.Vector4();
  private RPhiVP = new THREE.Vector4();
  private RhoZVP = new THREE.Vector4();

  // 3d objects
  tracks: THREE.Object3D;
  clusters: THREE.Object3D;
  combos: THREE.Object3D;
  axes: THREE.Group;
  detector: any;
  detector_rphi: any;
  detector_rhoz: any;

  @ViewChild('canvas', {static: true}) canvasRef: ElementRef;
  private get canvas() : HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  // particle materials
  private pimaterial: THREE.LineBasicMaterial;
  private kmaterial: THREE.LineBasicMaterial;
  private mumaterial: THREE.LineBasicMaterial;
  private ematerial: THREE.LineBasicMaterial;

  // stage properties 
  public fieldOfView: number = 70;
  public nearClippingPane: number = 0.01;
  public farClippingPane: number = 8000;

  // event properties
  private theevent: VSDEvent;
  theparticle: any;
  private idparticle: number;
  eventnumber:number = 0;
  private path = 'assets/events/';
  filename: string;
  private url: string;

  // mass calculation variables
  private particleCount: number = 0; 
  particlePos: any;
  particleNeg: any;
  particleBac: any;
  massCalculated: number;
  firstMass:boolean = false;  

  selectedView: string;

  // histogram properties
  @Input() private histogramdata:Array<number>;

  @ViewChild('kaon', {static: true}) kaonRef: ElementRef;
  @ViewChild('lambda', {static: true}) lambdaRef: ElementRef;
  @ViewChild('antilambda', {static: true}) antilambdaRef: ElementRef;
  @ViewChild('xi', {static: true}) xiRef: ElementRef;

  private histKaon: any;
  private massKaon: number[];
  private histLambda: any;
  private massLambda: number[];
  private histAntiLambda: any;
  private massAntiLambda: number[];
  private histXi: any;
  private massXi: number[];

  constructor(  private eventService: EventService, 
                private toastr: ToastrService, 
                private combinationService: CombinationService,
                private sanitizer: DomSanitizer,
                public dialog: MatDialog  ) {}

  // notifications
  Warning(e: string) {
    this.toastr.warning(e);
  }
  Info(e: string) {
    this.toastr.info(e);
  }
  Error(e: string) {
    this.toastr.error(e);
  }
  Success(e: string) {
    this.toastr.success(e);
  }

  /*
    getStudentValues(): save name, grade and combination input in homepage
  */
  getStudentValues() {
    this.combinationService.getCombi().subscribe(
      (data) => {
        this.currentStudent = data;
      },
      (error) => {
        console.log("Error "+error);
      });
  }


  /* 
    showEvent(): load json file, put results in a Particle[] structure and call createEvent() to draw tracks
  */
 showEvent(diff: number) {
    this.filename = 'event_'+this.currentStudent.combination+'_'+ (this.eventnumber+diff) +'.json';
    this.url = this.path + this.filename;

    this.eventService.getevent(this.url)
      .subscribe(
        (response) => {
          this.theevent = response;
          this.createEvent(this.theevent);
          this.eventnumber += diff;
        },
        (error) => {
          console.log("Error "+error);
          this.Success('You have successfully completed MasterClassDisplay exercise.');
          this.showEvent(0);
        }
        );
  }

  /*
    next(): allow to move on next event, load next json and draw tracks
  */
  next() {
    if ( this.eventnumber < 30 ) {
      this.zoomReset();
      this.clearParticles();
      this.clearScene();
      this.clearInfo();
      this.showEvent(+1);
    }   
  }

  /*
    previous(): allow to skip back to previous event, load previous json and draw tracks
  */
  previous() {
    if ( this.eventnumber >0 ) {
      this.zoomReset();
      this.clearParticles();
      this.clearScene();
      this.clearInfo();
      this.showEvent(-1);
    }
  }

  /*
   clearScene(): remove all tracks from scene
  */
  clearScene(){
    this.scene.remove(this.tracks);
    this.scene.remove(this.combos);
    this.scene.remove(this.clusters);
  }

  /*
    saveParticle(selected: Track): save two selected particles 
  */
  saveParticle(selected: Track) {
    let sign = selected.sign;
    let type = selected.type;

    if(type == 3) {//bachelor
      this.particleBac = selected;
    }
    else if (sign > 0) {
      this.particlePos = selected;
    }
    else if (sign < 0) {
      this.particleNeg = selected;
    }

    if(this.particlePos && this.particleNeg) {
      if(this.particlePos.type != 2) {
        this.calculateV0();
      } else if (this.particleBac) {
        this.calculateCascade();
      }
    }
  }

  /*
   calculate(): invariant mass calculation
  */
  calculateV0() {
    var a = (this.particlePos.E + this.particleNeg.E)*(this.particlePos.E + this.particleNeg.E);
    var b = (this.particlePos.px + this.particleNeg.px)*(this.particlePos.px + this.particleNeg.px);
    var c = (this.particlePos.py + this.particleNeg.py)*(this.particlePos.py + this.particleNeg.py);
    var d = (this.particlePos.pz + this.particleNeg.pz)*(this.particlePos.pz + this.particleNeg.pz);
    this.massCalculated = Math.sqrt(a-b-c-d);
  }

  /*
   calculateCascade(): invariant mass calculation
  */
  calculateCascade() {
    var a = (this.particlePos.E + this.particleNeg.E + this.particleBac.E)*(this.particlePos.E + this.particleNeg.E + this.particleBac.E);
    var b = (this.particlePos.px + this.particleNeg.px + this.particleBac.px)*(this.particlePos.px + this.particleNeg.px + this.particleBac.px);
    var c = (this.particlePos.py + this.particleNeg.py + this.particleBac.py)*(this.particlePos.py + this.particleNeg.py + this.particleBac.py);
    var d = (this.particlePos.pz + this.particleNeg.pz + this.particleBac.pz)*(this.particlePos.pz + this.particleNeg.pz + this.particleBac.pz);
    this.massCalculated = Math.sqrt(a-b-c-d);
  }

  /*
   clearParticles(): clear variables related to mass calculation
  */
  clearParticles(){
    this.particleNeg = null;
    this.particlePos = null;
    this.particleBac = null;
    delete this.massCalculated;
    this.particleCount = 0;
  }

  /*
   clearInfo(): clear 'Particle information' fieldset
  */
  clearInfo() {
    this.theparticle = null;
  }

  calculatedMassInBounds(histogram: any) {
    if(this.massCalculated > histogram.minx && this.massCalculated < histogram.maxx) {
      return true;
    } else {
      this.Warning('Incorrect mass for this particle type !');
      return false;
    }

  }

  /*
   add(): add mass calculated to histogram
  */
  add() {
    if ( !this.particlePos || !this.particleNeg) {
      this.Warning('You have to select two particles !');
      return;
    }

    if (!this.particleSelected) {
      this.Warning('You have to select the particle type !');
    }

    if (this.particleSelected == 1) {
      if(this.calculatedMassInBounds(this.histKaon)) {
        this.massKaon.push(this.massCalculated);
        this.updateHisto(this.histKaon, this.massKaon);
      }
    } else if (this.particleSelected == 2) {
      if(this.calculatedMassInBounds(this.histLambda)) {
        this.massLambda.push(this.massCalculated);
        this.updateHisto(this.histLambda, this.massLambda);
      }
    } else if (this.particleSelected == 3) {
      if(this.calculatedMassInBounds(this.histAntiLambda)) {
        this.massAntiLambda.push(this.massCalculated);
        this.updateHisto(this.histAntiLambda, this.massAntiLambda);
      }
    } else if (this.particleSelected == 4) {
      if(this.calculatedMassInBounds(this.histXi)) {
        this.massXi.push(this.massCalculated);
        this.updateHisto(this.histXi, this.massXi);
      }
    }
  }

  /* 
    createEvent(event: VSDEvent): draw tracks from json data 
  */
  createEvent( event: VSDEvent ){

    this.tracks = new THREE.Object3D;
    this.combos = new THREE.Object3D;
    this.clusters = new THREE.Object3D;

    const scale = 1.0e-2;

    //parse tracks
    for (let i in event.tracks) {
      let track = event.tracks[i].trajectory;
      let pID = event.tracks[i].particleId;

      let points = [];
      
      for ( let j in track ) {
        let point = track[j]; // coordinates x, y, z of a point
        points.push( new THREE.Vector3(scale*point[0], scale*point[1], scale*point[2]) ); // THREE.Vector3 is here a point in 3D space
      }

      let spline = new THREE.CatmullRomCurve3(points); // 3D spline curve from a series of points
      let vertex = spline.getPoints(200);
      let geometryLine = new THREE.BufferGeometry().setFromPoints( vertex );

      let line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x4169E1, linewidth: 1.1 } ), THREE.LineStrip );

      this.tracks.add(line);
    }

    this.tracks.visible = this.checkedTracks;

    this.scene.add(this.tracks);

    //parse combos (V0s and Cascades)
    for (let i in event.combos) {
      let combo = event.combos[i];

      let tuple = new THREE.Object3D;

      for (let j in combo) {
        let track = event.combos[i][j].trajectory;
        let sign = event.combos[i][j].sign;
        let mass = event.combos[i][j].mass;
        let comboId = event.combos[i][j].comboId;
        let px = event.combos[i][j].px;
        let py = event.combos[i][j].py;
        let pz = event.combos[i][j].pz;
        let type = event.combos[i][j].type;
  
        let points = [];
  
        for ( let k in track ) {
          let point = track[k]; // coordinates x, y, z of a point
          points.push( new THREE.Vector3(scale*point[0], scale*point[1], scale*point[2]) ); // THREE.Vector3 is here a point in 3D space
        }
  
        let spline = new THREE.CatmullRomCurve3(points); // 3D spline curve from a series of points
        let vertex = spline.getPoints(200);
        let geometryLine = new THREE.BufferGeometry().setFromPoints( vertex );

        let line;

        if(type == 3) { //bachelor from cascade
          line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x4169E1, linewidth: 1.1 } ));
        } else if(sign < 0) {
          line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x32CD32, linewidth: 3 } ));
        } else if(sign > 0) {
          line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0xFF4500, linewidth: 3 } ));
        } else {
          line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x4169E1, linewidth: 1.1 } ));
        }

        line.userData = event.combos[i][j];

        tuple.add(line);
      }
      this.combos.add(tuple);
      break;
    }

    this.combos.visible = this.checkedCombos;
    this.scene.add(this.combos);

    let points = [];

    for (let i in event.clusters) {
      const point = event.clusters[i];
      points.push( new THREE.Vector3(scale*point[0], scale*point[1], scale*point[2]) );
    }

    let geometryPoints = new THREE.BufferGeometry().setFromPoints( points );
    this.clusters = new THREE.Points(geometryPoints, new THREE.PointsMaterial({color: 0x9933ff, size: scale}));

    this.clusters.visible = this.checkedClusters;
    this.scene.add(this.clusters);

    this.firstMass = true; // mass can be added to histogram
  }

  /* 
   createScene(): add scene, camera, renderer and controls 
  */
  createScene() {
    // Scene 
    this.scene = new THREE.Scene();

    // Adding the axis
    const origin = new THREE.Vector3( 0, 0, 0 );
    const length = 1;
    const axiscolor = 0x0000ff;
    const headlength = 0.01;
    const headwidth = 0.001;
    this.axes = new THREE.Group();
    const zaxis = new THREE.ArrowHelper( new THREE.Vector3( 0, 0, 1 ), origin, length, axiscolor, headlength, headwidth );
    const xaxis = new THREE.ArrowHelper( new THREE.Vector3( 1, 0, 0 ), origin, length, axiscolor , headlength, headwidth);
    const yaxis = new THREE.ArrowHelper( new THREE.Vector3( 0, 1, 0 ), origin, length, axiscolor, headlength, headwidth );
    this.axes.add( zaxis );
    this.axes.add( xaxis );
    this.axes.add( yaxis );
    this.scene.add(this.axes);

    // Camera
    const aspectRatio = this.getAspectRatio();
    this.camera3d = new THREE.PerspectiveCamera(
      this.fieldOfView,
      aspectRatio,
      this.nearClippingPane,
      this.farClippingPane
    );
    this.camera3d.position.set( -7.5, 7.5, 2.5 );
    this.camera3d.up.set( 0.0, 1.0, 0.0 );

    this.camerarphi = new THREE.PerspectiveCamera(
      this.fieldOfView,
      aspectRatio,
      this.nearClippingPane,
      this.farClippingPane
    );
    this.camerarphi.position.set( 0.0, 0.0, 10.0 );
    this.camerarphi.up.set( 0.0, 1.0, 0.0 );
    this.camerarphi.lookAt(new THREE.Vector3(0, 0, 0));

    this.camerarhoz = new THREE.PerspectiveCamera(
      this.fieldOfView,
      aspectRatio,
      this.nearClippingPane,
      this.farClippingPane
    );
    this.camerarhoz.position.set( -10.0, 0.0, 0.0 );
    this.camerarhoz.up.set( 0.0, 1.0, 0.0 );
    this.camerarhoz.lookAt(new THREE.Vector3(0, 0, 0));

    // Renderer 
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, logarithmicDepthBuffer: true });
    this.renderer.setClearColor( 0xFFFFFF );
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);

    // Controls 
    this.controls = new OrbitControls( this.camera3d, this.renderer.domElement );
    this.controls.target.set( 0.0, 0.0, 0.0);
    this.controls.maxPolarAngle = 0.5*Math.PI;
    this.controls.autoRotate = this.autorotate;
    this.controls.update();
  }

  /*
   getAspectRatio(): retrun aspect ratio of the canvas
  */
  getAspectRatio() {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  /*
   zoomReset(): replace scene and camera at their initial position
  */
  zoomReset() {
    this.camera3d.position.set( -7.5, 7.5, 2.5 );
    this.camera3d.up.set( 0.0, 1.0, 0.0 );
    this.controls.reset();
    this.controls.target.set( 0.0, 0.0, 0.0);
    this.controls.maxPolarAngle = 0.5*Math.PI;
    this.controls.update();
  }

  /* 
   createLight(): add one ambient ligth and two directional ligths 
  */
  createLight(){
    var ambientLight = new THREE.AmbientLight(0x040404);
    this.scene.add(ambientLight);

    var directionalLighta = new THREE.DirectionalLight(0xffffff);
    directionalLighta.position.set(1.0, 1.0, 1.0);
    this.scene.add(directionalLighta);

    var directionalLightb = new THREE.DirectionalLight(0xffffff);
    directionalLightb.position.set(-1.0, 1.0, -1.0);
    this.scene.add(directionalLightb);
  }

  /*
    loaddetector(): load GLTF object
  */
  loaddetector(){
    const loader_gltf = new GLTFLoader();
    loader_gltf.load('assets/alice2.gltf', (gltf: GLTF) =>
    {
      this.detector = gltf.scene;
      this.setOpacityDetector(0.3);
      this.scene.add(this.detector);
    });

    const loader_svg = new SVGLoader();

    let finalize_s1 = (group) => {
      group.scale.set(0.18, 0.18, 0.18);
  
      group.renderOrder = -1; //force to be always rendered behind tracks
  
      this.detector_rphi = group;
  
      this.scene.add(this.detector_rphi);
    };

    let finalize_s2 = (group) => {
      group.rotateY(0.5*Math.PI);
      group.scale.set(0.15, 0.15, 0.15);
  
      group.renderOrder = -1; //force to be always rendered behind tracks
  
      this.detector_rhoz = group;
  
      this.scene.add(this.detector_rhoz);
    };
    loader_svg.load('assets/slice1.svg', (data: SVGResult) => {this.addSVGToScene(data, finalize_s1)});
    loader_svg.load('assets/slice2.svg', (data: SVGResult) => {this.addSVGToScene(data, finalize_s2)});
  }

  addSVGToScene(data: SVGResult, finalize: any) {
    const paths = data.paths;
    const group = new THREE.Group();

    for ( let i = 0; i < paths.length; i ++ ) {

      const path = <any>paths[ i ];

      let fillColor = path.userData.style.fill;

      const material = new THREE.MeshBasicMaterial( {
        color: new THREE.Color().setStyle(fillColor),
        side: THREE.DoubleSide,
        depthWrite: false
      } );

      const shapes = path.toShapes( true );

      for ( let j = 0; j < shapes.length; j ++ ) {

        const shape = shapes[ j ];
        const geometry = new THREE.ShapeBufferGeometry( shape );
        const mesh = new THREE.Mesh( geometry, material );
        group.add( mesh );
      }
    }
    finalize(group);
  }

  //If we had a model made of primitives...
  intersect() {
    const geometry = new THREE.BoxGeometry( 10, 10, 0.1 );
    const material = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.FrontSide} );
    const plane = new THREE.Mesh( geometry, material );

    this.detector.updateMatrix();
    plane.updateMatrix();

    this.detector.traverse((node) => {
      if (node instanceof THREE.Mesh) {
        const bspA = CSG.fromMesh(node);
        const bspB = CSG.fromMesh(plane);

        const bspResult = bspA.intersect(bspB);
        const meshResult = CSG.toMesh(bspResult, node.matrix);

        meshResult.material = node.material;
        //this.scene3d.add(meshResult);
      }
    });
  }

  /* 
   startRenderingLoop(): create a loop that causes the renderer to draw the scene every time the screen is refreshed 
  */
  startRenderingLoop() {
    let component: EventDisplayComponent = this;
    let oldVP = new THREE.Vector4();
    component.renderer.setScissorTest(true);

    let render = () => {
      requestAnimationFrame(render);

      component.resize();
      this.controls.update();
      component.renderer.getViewport(oldVP);

      let width = Math.ceil(oldVP.z*this.ED3D_width);
      let height = Math.ceil(oldVP.w*this.ED3D_height);

      this.THREEDVP.set(oldVP.x, oldVP.y, width, oldVP.w);
      this.RPhiVP.set(oldVP.x + width, oldVP.y + height, width, height);
      this.RhoZVP.set(oldVP.x + width, oldVP.y, width, height);

      let isVisible =  this.checkedDetector;

      if(this.detector)
        this.detector.visible = isVisible;
      if(this.detector_rphi)
        this.detector_rphi.visible = false;
      if(this.detector_rhoz)
        this.detector_rhoz.visible = false;
      if(this.axes)
        this.axes.visible = true;

      component.renderer.setViewport(this.THREEDVP);
      component.renderer.setScissor(this.THREEDVP);
      component.renderer.render(component.scene, component.camera3d);

      if(this.detector)
      this.detector.visible = false;
      if(this.detector_rphi)
        this.detector_rphi.visible = isVisible;
      if(this.detector_rhoz)
        this.detector_rhoz.visible = false;
      if(this.axes)
        this.axes.visible = false;

      component.renderer.setViewport(this.RPhiVP);
      component.renderer.setScissor(this.RPhiVP);
      component.renderer.render(component.scene, component.camerarphi);

      if(this.detector)
        this.detector.visible = false;
      if(this.detector_rphi)
        this.detector_rphi.visible = false;
      if(this.detector_rhoz)
        this.detector_rhoz.visible = isVisible;
      if(this.axes)
        this.axes.visible = false;

      component.renderer.setViewport(this.RhoZVP);
      component.renderer.setScissor(this.RhoZVP);
      component.renderer.render(component.scene, component.camerarhoz);

      component.renderer.setViewport(oldVP);
      component.renderer.setScissor(oldVP);
    };

    render();
  }

  resize() {
    let parent = this.canvas.parentElement;
    let displayWidth = parent.clientWidth;
    let displayHeight = parent.clientHeight;

    if(this.canvas.width != displayWidth || this.canvas.height != displayHeight) {
      this.renderer.setSize(displayWidth, displayHeight);
      this.camera3d.aspect = (this.canvas.clientWidth*this.ED3D_width) / this.canvas.clientHeight;
      this.camerarphi.aspect = (this.canvas.clientWidth*this.ED3D_width) / (this.canvas.clientHeight*this.ED3D_height);
      this.camerarhoz.aspect = (this.canvas.clientWidth*this.ED3D_width) / (this.canvas.clientHeight*this.ED3D_height);

      this.camera3d.updateProjectionMatrix();
      this.camerarphi.updateProjectionMatrix();
      this.camerarhoz.updateProjectionMatrix();
    }
  }

  public findIntersect( event: MouseEvent) {
    let intersects = [];
    if (this.combos && this.checkedCombos) {
      let zoomz = this.controls.target.distanceTo( this.controls.object.position );
      let raycaster = new THREE.Raycaster();
      raycaster.params.Line.threshold = zoomz / 150;

      //raycaster.params.Line.threshold = 0.000008;

      const windowOffset = this.renderer.domElement.getBoundingClientRect();

      const viewportclick = new THREE.Vector2(
        event.clientX - windowOffset.x,
        -(event.clientY - windowOffset.y) + this.renderer.domElement.clientHeight
      );

      for(let v of [{view: this.THREEDVP, cam: this.camera3d}, {view: this.RPhiVP, cam: this.camerarphi}, {view: this.RhoZVP, cam: this.camerarhoz}]) {
        const vp = v.view;
        const cam = v.cam;
        const ndcclick = new THREE.Vector2(
          ((viewportclick.x - vp.x)/vp.z - 0.5) * 2,
          ((viewportclick.y - vp.y)/vp.w - 0.5) * 2
        );

        if(ndcclick.x < -1 || ndcclick.x > 1)
          continue;

        if(ndcclick.y < -1 || ndcclick.y > 1)
          continue;

        raycaster.setFromCamera(ndcclick, cam);

        if ( this.combos.children )
          intersects.push(...raycaster.intersectObjects( this.combos.children, true ));
      }
    }
    return intersects;
  }

  /* 
   onMouseDown( event: MouseEvent): allow selection of tracks and mass calculation of two consecutive selection
  */
  public onMouseDown( event: MouseEvent) {
    event.preventDefault();
    let intersects = this.findIntersect(event);
    if (intersects.length > 0) {
      if ( this.INTERSECTED != intersects[0].object ) {
        if (this.INTERSECTED ) this.INTERSECTED.material.linewidth = 1;
        this.INTERSECTED = intersects[0].object;
        this.INTERSECTED.material.linewidth = 4; // linewith of the current object increased from 1 to 4
      }else{ // line picked is the same as previous one
        this.INTERSECTED.material.linewidth = 1; 
      }
      this.saveParticle(this.theparticle); // particle picked is automatically saved
    }
  }

  /* 
   onMouseMove( event: MouseEvent ):  highlight track when mouse move over scene 
  */
  public onMouseMove( event: MouseEvent ){
    event.preventDefault();
    let intersects = this.findIntersect(event);
    if (intersects.length > 0) {
      if ( this.INTERSECTED2 != intersects[0].object ) {
        if (this.INTERSECTED2) this.INTERSECTED2.material.color.setHex(this.INTERSECTED2.currentHex);
        this.INTERSECTED2 = intersects[0].object;
        this.INTERSECTED2.currentHex = this.INTERSECTED2.material.color.getHex();
        this.INTERSECTED2.material.color.setHex(0xDEDE00); // color change
        let x = intersects[0].object;
        this.theparticle = x.userData;
      }
    } else {
      if (this.INTERSECTED2) this.INTERSECTED2.material.color.setHex(this.INTERSECTED2.currentHex);
      this.INTERSECTED2 = null;
    }
  }

  /*
   onChangeView(): change camera position
  */
  public onChangeView(){
    if( this.selectedView == 'top') {
      this.camera3d.position.set( 0.0, 10.0, 0.0 );
      this.camera3d.up.set( 0.0, 1.0, 0.0 );
    }
    if( this.selectedView == 'side') {
      this.camera3d.position.set( -10.0, 0.0, 0.0 );
      this.camera3d.up.set( 0.0, 1.0, 0.0 );
    }
    if( this.selectedView == 'front') {
      this.camera3d.position.set( 0.0, 0.0, 10.0 );
      this.camera3d.up.set( 0.0, 1.0, 0.0 );
    }
    if( this.selectedView == 'perspective') {
      this.camera3d.position.set( -7.5, 7.5, 2.5 );
      this.camera3d.up.set( 0.0, 1.0, 0.0 );
    }
    this.camera3d.updateProjectionMatrix();
    this.controls.update();
  }

  /*
   onChangeOpacity(): react when slider value is modified 
  */
  public onChangeOpacity( event ) {
    this.setOpacityDetector(event.value); 
  }

  /*
   onChangeRotate( event ): react when checkbox is modified
  */
  public onChangeRotate( event ) {
    this.autorotate = event.checked;
    this.controls.autoRotate = this.autorotate;
  }

  public onChangeZoomRPhi( event ) {
    this.camerarphi.zoom = event.value;
    this.camerarphi.updateProjectionMatrix();
  }

  public onChangeZoomRhoZ( event ) {
    this.camerarhoz.zoom = event.value;
    this.camerarhoz.updateProjectionMatrix();
  }

  /*
   setOpacityDetector( value ): change detector opacity
  */
  public setOpacityDetector( value ){
    this.detector.traverse( function (o) {
      if ( o.isMesh === true ) {
        o.material.transparent = true;
        o.material.opacity = value;
        o.material.side = THREE.DoubleSide;
      }
    });
  }

  /*
   hideDetChange(value: MatSlideToggleChange): display or remove detector from scene
  */
  hideDetChange(value: MatSlideToggleChange) {
    this.checkedDetector = value.checked;
    this.detector.visible = this.checkedDetector;
  }

  /*
   hideTracksChange(value: MatSlideToggleChange): display or remove tracks from scene
  */
  hideTracksChange(value: MatSlideToggleChange) {
    this.checkedTracks = value.checked;
    this.tracks.visible = this.checkedTracks;
  }

  /*
   hideCombosChange(value: MatSlideToggleChange): display or remove combos (V0s + Cascades) from scene
  */
  hideCombosChange(value: MatSlideToggleChange) {
    this.checkedCombos = value.checked;
    this.combos.visible = this.checkedCombos;
  }
  /*
   hideClustersChange(value: MatSlideToggleChange): display or remove Clusters from scene
  */
 hideClustersChange(value: MatSlideToggleChange) {
    this.checkedClusters = value.checked;
    this.clusters.visible = this.checkedClusters;
  }

  createHistogram(mount: any, minx: number, maxx: number, data) {
    let margin = {top: 10, right: 10, bottom: 50, left: 30};
    let width = 600 - margin.left - margin.right;
    let height = 320 - margin.top - margin.bottom;

    let svg = d3.select(mount)
                  .append("svg")
                  .attr("preserveAspectRatio", "xMinYMin meet")
                  .attr("viewBox", `0 0 ${width} ${height}`)
                  .attr("width", "100%")

  
    // // chart plot area
    let chart = svg.append('g')
      .attr('transform', `translate(${margin.left}, ${margin.top})`);

    // X axis: scale and draw
    let xScale = d3.scaleLinear<number>()
      .range([0, width - margin.left - 20])
      .domain([minx, maxx]);

    let xAxis = chart.append("g")
      .attr("transform", `translate(${margin.left},${height-margin.bottom})`)
      .call(d3.axisBottom(xScale));

    chart.append("g").append("text")
      .style("font-size", "13px")
      .attr('x', width-175)
      .attr('y', height-margin.bottom+30)
      .html('Invariant Mass (GeV/<tspan font-style="italic">c</tspan><tspan dy="-3">2</tspan><tspan dy="3">)</tspan>');
  
    // Y axis: scale and draw
    let yScale = d3.scaleLinear<number>()
      .range([height - margin.bottom , 0])
      .domain([0, 1])

    let yAxis = chart.append("g")
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(yScale));
      
    // // get data  
    let datagenerator = d3.histogram<number, number>()
      .domain(xScale.domain())
      .thresholds(xScale.ticks(40));

    let bins = datagenerator(data);
  
    let barWidth = xScale(bins[0].x1) - xScale(bins[0].x0) - 1;

    chart.selectAll(".bar")
      .data(bins)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("transform", d => {
        return "translate(" + xScale(d.x1) + "," + yScale((d.length)) + ")";
      })
      .attr("x", 1)
      .attr("width", barWidth)
      .attr("height", d =>  { return (height - margin.bottom - 20) - yScale(d.length); })
      .attr("fill", "#4169E1");

    chart.append("g")
        .attr("transform", `translate(0,0)`)
        .append("text")
        .attr("transform", "rotate(-90)")
        .style("font-size", "13px")
        .attr("text-anchor", "end")
        .text("Counts");

    // chart.append("g")
    //   .attr("class", "axis axis--x")
    //   .attr("transform", "translate(0," + (height - margin.bottom - 20) + ")")
    //   .call(d3.axisBottom(xScale))

      
    // yAxis
    //   .call(d3.axisLeft(yScale))
    //   .append("text")
    //   .attr("transform", "rotate(-90)")
    //   .attr("y", 10)
    //   .attr("dy", "-5.1em")
    //   .attr("text-anchor", "end")
    //   .attr("stroke", "black")
    //   .text("D0 Candidates (0.5 MeV");

    let info = chart.insert("g").attr("class", "infoMath")
                        .append("text", "hist");     

    return {margin: margin, width: width, height: height, svg: svg, chart: chart, xScale: xScale, yScale: yScale, xAxis: xAxis, yAxis: yAxis, minx: minx, maxx: maxx, info: info};
  }

  /*
   drawHisto(data): display of saved mass
  */
  drawHisto() {
    this.massKaon = [];
    this.massLambda = [];
    this.massAntiLambda = [];
    this.massXi = [];

    this.histKaon = this.createHistogram(this.kaonRef.nativeElement, 0.4, 0.6, this.massKaon);
    this.histLambda = this.createHistogram(this.lambdaRef.nativeElement, 1.0, 1.4, this.massLambda);
    this.histAntiLambda = this.createHistogram(this.antilambdaRef.nativeElement, 1.0, 1.4, this.massAntiLambda);
    this.histXi = this.createHistogram(this.xiRef.nativeElement, 1.0, 1.4, this.massXi);
    //this.histLambda = this.createHistogram('#lambda', 'Lambdas', 1.2, 1.4, this.massLambda);
    //this.histAntiLambda = this.createHistogram('#anti-lambda', 'Anti-Lambdas', 1.0, 1.2, this.massAntiLambda);
    //this.histXi = this.createHistogram('#xi', 'Xis', 1.0, 1.2, this.massXi);    
  }

  /*
   updateHisto( histogram, data ): when a mass is added, histogram is updated
  */
  updateHisto(histogram: any, data: any) {
    // get data   
    let datagenerator = d3.histogram<number, number>()
      .domain(histogram.xScale.domain())
      .thresholds(histogram.xScale.ticks(40));

    let bins = datagenerator(data);

    // // update yAxis
    histogram.yScale.domain([0, d3.max(bins, d => { return d.length; })]);
    histogram.yAxis.transition().duration(500).call(d3.axisLeft(histogram.yScale))

    // // update data 
    let barWidth = histogram.xScale(bins[0].x1) - histogram.xScale(bins[0].x0) - 1;

    histogram.chart.selectAll(".bar")
      .data(bins)
      .enter()
      .append("rect")
      .merge(histogram.chart.selectAll(".bar").data(bins))
      .transition()
      .duration(500)
      .attr("class", "bar")
      .attr("transform", d => {
        return "translate(" + histogram.xScale(d.x1) + "," + histogram.yScale(d.length) + ")";
      })
      .attr("x", 1)
      .attr("width", barWidth)
      .attr("height", d => { return histogram.height - histogram.margin.bottom - histogram.yScale(d.length); })
      .attr("fill", "#4169E1");

    // if(d3.deviation(data)) var deviation = d3.deviation(data).toFixed(3);

      histogram.info
        .enter()
        .append("text")
        .merge(histogram.info)
        .attr("x", 450 - histogram.margin.left)
        .attr("y", 10)
        .text("Entries "+ data.length);
    // this.infoHisto
    //   .enter()
    //   .append("text")
    //     .merge(this.infoHisto)
    //     .attr("y", 50)
    //     .attr("x", 270)
    //     .text("Entries "+ data.length) // Entries: number of mass added
    //     .append("tspan")
    //       .attr("y", 65)
    //       .attr("x", 270)
    //       .text("Means: "+ d3.mean(data).toFixed(3))
    //       .append("tspan")
    //         .attr("y", 80)
    //         .attr("x", 270)
    //         .text("Std dev: "+ deviation);
  }

  /*
    generateDownloadJsonUrl( data ): allow to export/download JSON file formed of calculated masses
  */
  downloadJsonHref;
  generateDownloadJsonUrl( data ) {
    var theJSON = JSON.stringify( data );
    var nameJSON = "result_"+this.currentStudent.combination+".json";
    var url = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
    this.downloadJsonHref = url;
  }

  openInstructions() {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '70%',
      data: {}
    });
  }

  /*  
   principal function
  */
  public ngOnInit() {
    this.getStudentValues();
    this.createScene();
    this.loaddetector();
    this.createLight();
    this.showEvent(0);
    this.startRenderingLoop();
    this.drawHisto();
  }
}