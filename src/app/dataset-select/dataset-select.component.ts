/**
* (c) Copyright 2019 CERN                                                     
*                                                                             
* This software is distributed under the terms of the GNU General Public      
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   
*                                                                             
* In applying this licence, CERN does not waive the privileges and immunities 
* granted to it by virtue of its status as an Intergovernmental Organization  
* or submit itself to any jurisdiction.                                       
*/

import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Student, CombinationService } from '../services/combination.service';
import { MatSliderModule } from '@angular/material/slider';
import {MatFormFieldModule} from '@angular/material/form-field';


@Component({
  selector: 'dataset-select',
  templateUrl: './dataset-select.component.html',
  styleUrls: ['./dataset-select.component.css']
})
export class DatasetSelectComponent implements OnInit {
  title: string = "Please select dataset for Visual Analysis:";
  
  currentStudent = {} as Student;
  isComplete = false;
  authStatus: boolean;
  authForm: FormGroup;
  formBuilder = new FormBuilder;

  constructor( private combinationService: CombinationService, private router: Router ) {}

  onSubmit(){
    this.currentStudent.combination = this.authForm.controls.combination.value;
  	this.sendCombi(this.currentStudent);
    this.isComplete = true;
    this.router.navigate(['/event-display']);
  }

  sendCombi(value: Student) {
  	this.combinationService.sendCombi(value);
  }

  ngOnInit() {	
    this.authStatus = this.combinationService.isAuth;

    if(this.authStatus){

      this.combinationService.getCombi().subscribe(
      (data) => {
        this.currentStudent = data;
      },
      (error) => {
        console.log("Error "+error);
      });

      this.authForm = this.formBuilder.group({
        'combination': [this.currentStudent.combination]
      })
      this.isComplete = true;

    }else {
      this.authForm = this.formBuilder.group({
        'combination': ['', Validators.required]
      })
    }

  }
}
