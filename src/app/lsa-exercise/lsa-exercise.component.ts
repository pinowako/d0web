/**
* (c) Copyright 2019 CERN                                                     
*                                                                             
* This software is distributed under the terms of the GNU General Public      
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   
*                                                                             
* In applying this licence, CERN does not waive the privileges and immunities 
* granted to it by virtue of its status as an Intergovernmental Organization  
* or submit itself to any jurisdiction.                                       
*/

import { Component, OnInit, OnChanges, ElementRef, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { LSAdataService } from '../services/lsadata.service';
import { ToastrService } from 'ngx-toastr' ;
import * as d3 from 'd3';
import { regressionExp } from 'd3-regression';
import { nelderMead, conjugateGradient } from 'fmin';
import { MatProgressButtonOptions } from 'mat-progress-buttons';
import { NouiFormatter } from 'ng2-nouislider';
import { Observable, of } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { ModalComponent } from '../modal/modal.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export type DataType = {x:any, y:any};

@Component({
  selector: 'lsa-exercise',
  templateUrl: './lsa-exercise.component.html',
  styleUrls: ['./lsa-exercise.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LSAExerciseComponent implements OnInit {
  title: string = "Large Scale Analysis";

  // charts 
  private url: string;
  private urlHints: string;
  private currentHist: string;
  private InvMassChart;
  private signalPath;
  private backgroundPath;
  private xScale;
  private yScale;
  private xAxis;
  private yAxis;
  private xmin;
  private xmax;
  zoom_xmin;
  zoom_xmax;
  private ymin;
  private ymax;
  private histHeight;
  private mainArea;
  private margin;
  private bins;
  private brush;
  private idleTimeout;
  private infoMath;

  //Display up to 3 decimals in the tooltip
  formatter: NouiFormatter = {

    from(value: string): number {
      let v = Number(value);
      return Math.round (v * 1000) / 1000;
    },
  
    to(value: number): string {
      let v = Math.round (value * 1000) / 1000;
      return String(v);
    }
  
  };

  hist_data: any;
  fit_hint: number[];
  fit_current: number[];
  fit_current_binrange: any;
  fit_points_full: object[];
  fit_points_bckg: object[];
  fit_points_total: number = 256; //how "jagged" the fit line will be. With 256 sample points looks good enough for me

  // private info;
  // private fit;
  // private propaBkg;
  interval: number;
  // fitResult = [];
  // meanLifetime = [];
  // fitError;
  // fitErrorMean;
  // fitErrorSigma;
  // valLifetime;
  // errLifetime;

  // initial_fit = [];
  // fit_data;
  // initial: Boolean = true;

  // // boolean
  
  // // forms
  rangeSignal;
  rangeBackground;

  selectedParticle: any;
  selectedHistogram: any;
  finishedHistograms: any;

  constructor(  private LSAdataService: LSAdataService, 
                private toastr: ToastrService, 
                public dialog: MatDialog ) { }

  /*
   notifications 
  */
  Info( e: string ) { 
    this.toastr.info(e); 
  }

  Success( e: string ) { 
    this.toastr.success(e); 
  }

  /*
    showdata(): load JSON file, initialise bins and plot D0 mass distribution
  */
  showdata() {
    this.fit_current = null;
    this.fit_current_binrange = null;

    if(this.InvMassChart) {
      this.signalPath.attr("d", "");
      this.backgroundPath.attr("d", "");
      this.fit_points_bckg = [];
      this.fit_points_full = [];
      this.infoMath.text("");
    }

    this.url = `assets/invariant/${this.selectedHistogram}_${this.selectedParticle}.json`;
    this.urlHints = `assets/invariant/${this.selectedHistogram}_${this.selectedParticle}_hint.json`;
    this.currentHist = `${this.selectedHistogram}_${this.selectedParticle}`;
    
    this.LSAdataService.getdata(this.url)
    .subscribe(
      (response) => {
        this.hist_data = response;
        this.plotMass(this.hist_data, ".histo_MM");
        // this.displayMass = false;
      },
      (error) => {
        console.log("Error "+error);
      }
      );

      this.LSAdataService.gethints(this.urlHints)
      .subscribe(
        (response) => {
          this.fit_hint = response;
        },
        (error) => {
          console.log("Error "+error);
        }
        );
  }

  /*
    onSubmitDist(distform : NgForm): submit form with min and max range variables input and display variable distributions
  */
  onSubmitDist( distform : NgForm ) {
    // if ( this.displayFit ) {
    //   this.minRangeGaus = distform.form.controls.range.value[0];
    //   this.maxRangeGaus = distform.form.controls.range.value[1];

    //   this.minRangeDistIP = distform.form.controls.IP.value[0];
    //   this.maxRangeDistIP = distform.form.controls.IP.value[1];

    //   this.minRangeDistPT = distform.form.controls.PT.value[0];
    //   this.maxRangeDistPT = distform.form.controls.PT.value[1];

    //   this.minRangeDistTAU = distform.form.controls.TAU.value[0];
    //   this.maxRangeDistTAU = distform.form.controls.TAU.value[1];

    //   if( this.minRangeGaus == 1810 && this.maxRangeGaus == 1915 ){
    //     this.Info('You cannot set the signal range to be the same as the total masse range.');
    //   } else {
    //     this.plotDistribution();
    //   }
    // } else {
    //   this.Info("You must fit mass distribution before.");
    // }
  }

  createInvMassChart(divName) {
    this.margin = {top: 20, right: 20, bottom: 40, left: 60};
    let width = 560 - this.margin.left - this.margin.right;
    this.histHeight = 400 - this.margin.top - this.margin.bottom;

    let margin = this.margin;
        
    // // Append SVG            
    let svg = d3.select(divName)
                .append("svg")
                .attr("preserveAspectRatio", "xMinYMin meet")
                .attr("width", "100%")
                .attr("viewBox", `0 0 ${width} ${this.histHeight}`);

    this.InvMassChart = svg.append("g")
        .attr('transform', `translate(${margin.left}, ${margin.top})`);

    let clip = this.InvMassChart.append("defs").append("svg:clipPath")
      .attr("id", "clip")
      .append("svg:rect")
      .attr("width", width )
      .attr("height", this.histHeight )
      .attr("x", 0)
      .attr("y", 0);

    this.brush = d3.brushX()
      .extent( [ [0,0], [width,this.histHeight] ] )
      .on("end", () => {this.onZoom()})

    this.mainArea = this.InvMassChart.append('g')
      .attr("clip-path", "url(#clip)")

    // X axis
    this.xScale = d3.scaleLinear<number>().rangeRound([0, width - margin.left - 20]);
    this.xScale.domain([this.xmin, this.xmax]);
    this.xAxis = this.InvMassChart.append("g").attr("transform", "translate(0," + (this.histHeight - margin.bottom - 20) + ")").call(d3.axisBottom(this.xScale));

    let text = this.xAxis.append("text")
          .attr("transform", `translate(${2*width/3}, ${margin.bottom - 10})` )
          .attr("stroke", "black")
          .html('Invariant Mass (GeV/<tspan font-style="italic">c</tspan><tspan dy="-3">2</tspan><tspan dy="3">)</tspan>');

    // Y axis: scale and draw
    this.yScale = d3.scaleLinear<number>().range([this.histHeight - margin.left , 0]);
    this.yScale.domain([this.ymin, this.ymax]);
    this.yAxis = this.InvMassChart.append('g').attr("class", "yAxis").call(d3.axisLeft(this.yScale));

    this.yAxis.append("g")
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "-5.1em")
          .attr("text-anchor", "end")
          .attr("stroke", "black")
          .text("Counts");

    // this.mainArea.selectAll(".bar")
    //   .data(this.bins)
    //   .enter().append("rect")
    //   .attr("class", "bar")
    //   .attr("transform", d => { return `translate(${this.xScale(d[0] - this.interval/2)},${this.yScale(d[1])})`})
    //   .attr("x", 1)
    //   .attr("width", d =>  { return Math.max(1, this.xScale(d[0] + this.interval) - this.xScale(d[0])); })
    //   .attr("height", d => {return (this.histHeight - margin.bottom - 20) - this.yScale(d[1]); })
    //   .attr("fill", "#A9A9A9");

    let barWidth = Math.max(Math.ceil(this.xScale(this.bins[0].x1) - this.xScale(this.bins[0].x0)), 1);

    this.mainArea.selectAll(".bar")
      .data(this.bins)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("transform", d => { return `translate(${this.xScale(d.x0) - (this.xScale(d.x1) - this.xScale(d.x0))/2},${this.yScale(d.length)})`})
      .attr("x", 1)
      .attr("width", barWidth)
      .attr("height", d =>  { return (this.histHeight - margin.bottom - 20) - this.yScale(d.length); })
      .attr("fill", "#A9A9A9");

    this.mainArea.append("g")
      .attr("class", "brush")
      .call(this.brush);

    this.signalPath = this.mainArea.append("path");
    this.backgroundPath = this.mainArea.append("path");

    this.signalPath
    .attr("d", "")
    .attr("stroke", "#F00000")
    .attr("fill-opacity","0")
    .attr("stroke-width", 1.5)
    .attr("stroke-linejoin", "round");

    this.backgroundPath
    .attr("d", "")
    .attr("stroke", "#0066CC")
    .attr("fill-opacity","0")
    .attr("stroke-width", 1.5)
    .attr("stroke-linejoin", "round");

    this.infoMath = this.InvMassChart.insert("g").attr("class", "infoMath").append("text", "hist");
  }

  idled() {
    this.idleTimeout = null;
  }

  onZoom() {
    let extent = d3.event.selection;

    if(!extent){
      if (!this.idleTimeout) return this.idleTimeout = setTimeout(() => {this.idled();}, 350);
      this.updateInvMassChart(this.xmin, this.xmax);

      this.zoom_xmin = this.xmin;
      this.zoom_xmax = this.xmax;
    }else{
      this.zoom_xmin = this.xScale.invert(extent[0]);
      this.zoom_xmax = this.xScale.invert(extent[1]);
      
      this.updateInvMassChart(this.xScale.invert(extent[0]), this.xScale.invert(extent[1]));
      this.mainArea.select(".brush").call(this.brush.move, null);
    }

    let line = d3.line()
    .x( d => {return this.xScale(d.x);} )
    .y( d => {return this.yScale(d.y);} );

    this.signalPath
      .transition()
      .duration(500)
      .attr("d", line(this.fit_points_full))

    this.backgroundPath
      .transition()
      .duration(500)
      .attr("d", line(this.fit_points_bckg))
  }

  updateInvMassChart(xdomain_start, xdomain_end) {
    this.yScale.domain([this.ymin, this.ymax]);
    this.yAxis.transition().duration(500).call(d3.axisLeft(this.yScale));

    this.xScale.domain([xdomain_start, xdomain_end]);
    this.xAxis.transition().duration(500).call(d3.axisBottom(this.xScale));

    let barWidth = Math.max(Math.ceil(this.xScale(this.bins[0].x1) - this.xScale(this.bins[0].x0)), 1);

    this.mainArea.selectAll(".bar")
    .data(this.bins)
    .enter()
    .append("rect")
    .merge(this.mainArea.selectAll(".bar").data(this.bins))
    .transition()
    .duration(500)
    .attr("class", "bar")
    .attr("transform", d => { return `translate(${this.xScale(d.x0) - (this.xScale(d.x1) - this.xScale(d.x0))/2},${this.yScale(d.length)})`; })
    .attr("x", 1)
    .attr("width", d =>  barWidth)
    .attr("height", d => { return (this.histHeight - this.margin.bottom - 20) - this.yScale(d.length); })
    .attr("fill", "#A9A9A9");
  }

  acceptFit() {
    let signal = this.rangeSignal;
    let background = this.rangeBackground;

    let signal_start = signal[0];
    let signal_end = signal[1];

    let background_start = background[0];
    let background_end = background[1];

    let background_binrange = this.find_bin_range(background_start, background_end);
    let signal_binrange = this.find_bin_range(signal_start, signal_end);
    let count = background_binrange.xbin_end - background_binrange.xbin_start;

    let countTotal = 0, countSignal = 0, countBackground = 0, errSignal = 0;

    let poly_p = [this.fit_current[3], this.fit_current[4], this.fit_current[5]];

    for(let i = signal_binrange.xbin_start; i < signal_binrange.xbin_end; i++) {
        countTotal += this.bins[i].length;
        countBackground += this.modelPoly(poly_p, this.bins[i].x0)[0];
    }

    countBackground = Math.floor(countBackground);

    countSignal = countTotal - countBackground;
    errSignal = Math.round(Math.sqrt(countSignal));

    let invBinWidth = 1 / (this.bins[0].x1-this.bins[0].x0);

    let alpha = this.fit_current[0];
    let mu = this.fit_current[1];
    let sigma = this.fit_current[2];
    let intSignal = Math.round(alpha*invBinWidth);

    let alpha_test = alpha;
    let alpha_min = alpha;

    let chi2_loss = (p) => {
      let res = 0;

      for (let i=0; i < count; i++) {
        let index = i+background_binrange.xbin_start;
        let observed = this.bins[index].length;
        let expected = this.modelGaussAlphaPolyFixed(p, this.bins[index].x0, alpha_test, poly_p)[0];
        let diff = observed - expected;
        res += (diff*diff) / Math.max(observed, 1);
      }
      return res;
    }

    let p0_gauss = [mu, sigma];
    let chi2_min = chi2_loss(p0_gauss);

    // first guess
    let alpha_diff = Math.sqrt(invBinWidth*alpha)/invBinWidth;
    alpha_test = alpha + alpha_diff;
    nelderMead(chi2_loss, p0_gauss);
    let chi2_test = chi2_loss(p0_gauss);

    // find upper bound on alpha
    for (let maxit = 0; maxit < 20; maxit++) {
      if (chi2_test >= chi2_min + 1.)
        break;
      alpha_min = alpha_test;
      alpha_test = alpha + 2. * (alpha_test - alpha);
      nelderMead(chi2_loss, p0_gauss);
      chi2_test = chi2_loss(p0_gauss);
    }

    let alpha_max = alpha_test;

    // iterate
    for (let maxit = 0; maxit < 20; maxit++) {
      if ((alpha_max - alpha_min)*invBinWidth <= 0.5)
        break;
      alpha_test = 0.5 * (alpha_max + alpha_min);
      nelderMead(chi2_loss, p0_gauss);
      chi2_test = chi2_loss(p0_gauss);
      if (chi2_test < chi2_min + 1.) {
        alpha_min = alpha_test;
      }
      else {
        alpha_max = alpha_test;
      }
    }

    let alpha_up = 0.5 * (alpha_max + alpha_min);

    //downward direction
    // first guess
    alpha_min = alpha;
    alpha_test = 2*alpha - alpha_up;
    nelderMead(chi2_loss, p0_gauss);
    chi2_test = chi2_loss(p0_gauss);

    // find upper bound on alpha
    for (let maxit = 0; maxit < 20; maxit++) {
      if (chi2_test >= chi2_min + 1.)
        break;
        alpha_min = alpha_test;
        alpha_test = alpha + 2. * (alpha_test - alpha);
        nelderMead(chi2_loss, p0_gauss);
        chi2_test = chi2_loss(p0_gauss);
    }

    alpha_max = alpha_test;

    // iterate
    for (let maxit = 0; maxit < 20; maxit++) {
      if ((alpha_min - alpha_max)*invBinWidth <= 0.5)
        break;
      alpha_test = 0.5 * (alpha_max + alpha_min);
      nelderMead(chi2_loss, p0_gauss);
      chi2_test = chi2_loss(p0_gauss);
      if (chi2_test < chi2_min + 1.) {
        alpha_min = alpha_test;
      }
      else {
        alpha_max = alpha_test;
      }
    }

    let alpha_low = 0.5 * (alpha_max + alpha_min);
    errSignal = Math.round(0.5 * (alpha_up - alpha_low) * invBinWidth);

    this.infoMath
    .enter()
    .append("text")
      .merge(this.infoMath)
      .attr('x', 270)
      .attr('y', 20)
      .style("font-size", "11px")
      .text(`Total: ${countTotal}`)
       .append("tspan")
        .attr('x', 270)
        .attr('y', 30)
        .text(`Background: ${countBackground}`)
            .append("tspan")
            .attr('x', 270)
            .attr('y', 40)
            .text(`Signal: ${countSignal} ± ${errSignal}`)
              // .append("tspan")
              // .attr('x', 270)
              // .attr('y', 50)
              // .text("Mean : "+ p1[2].toFixed(1)  + ' ± ' + this.fitErrorMean.toFixed(1))
              //     .append("tspan")
              //     .attr('x', 270)
              //     .attr('y', 60)
              //     .text("Deviation : "+ p1[1].toFixed(1) + ' ± ' + this.fitErrorSigma.toFixed(1));

  }

  /*
   plotMass( data ): display D0 mass distribution
  */
  plotMass( data, divName ) {
    this.xmin = data.xmin;
    this.xmax = data.xmax;

    this.margin = {top: 20, right: 20, bottom: 40, left: 60};
    let width = 560 - this.margin.left - this.margin.right;
    this.histHeight = 400 - this.margin.top - this.margin.bottom;

    let xScale = d3.scaleLinear<number>()
      .rangeRound([0, width - this.margin.left - 20])
      .domain([this.xmin, this.xmax]);

    let datagenerator = d3.histogram<number, number>()
      .domain(xScale.domain())
      .thresholds(xScale.ticks(data.bins));

    this.bins = datagenerator(this.hist_data.data);

    this.ymin = 0;
    this.ymax = d3.max(this.bins, d => {return d.length});

    if(!this.InvMassChart) {
      this.createInvMassChart(divName);
    } else {
      this.updateInvMassChart(this.xmin, this.xmax);
    }
  }

  // Make sure that an object acts as an array (even if it's a scalar).
  atleast_1d(x) {
    if (typeof(x.length) === "undefined") {
      var tmp = new Array();
      tmp[0] = x;
      return tmp;
    }
    return x;
  }

  modelPoly(a, x) {
    x = this.atleast_1d(x);
    a = this.atleast_1d(a);

    let result = [];

    for(let i = 0; i < x.length; i++) {
      for(let j = 0; j < a.length; j++) {
        if(j == 0) {
          result.push(a[j]);
        } else {
          result[i] += a[j] * Math.pow(x[i], j);
        }
      }
    }
    return result;
  }

  modelGauss(a, x) {
    x = this.atleast_1d(x);
    a = this.atleast_1d(a);

    let alpha = a[0];
    let mu = a[1];
    let sigma = a[2];

    let result = [];
    let norm = alpha / (Math.sqrt(2*Math.PI)*sigma);

    for(let i = 0; i < x.length; i++) {
      let diff = x[i] - mu;
      diff = diff / sigma;
      result.push(norm * Math.exp(-0.5 * diff * diff));
    }

    return result;
  }

  modelGaussPoly(a, x) {
    let a_gauss = [a[0], a[1], a[2]];
    let a_poly = [a[3], a[4], a[5]];
    let res1 = this.modelGauss(a_gauss, x);
    let res2 = this.modelPoly(a_poly, x);

    let result = [];

    for(let i = 0; i < res1.length; i++) {
      result.push(res1[i] + res2[i]);
    }

    return result;
  }

  modelGaussPolyFixed(a, x, a_poly) {
    let a_gauss = [a[0], a[1], a[2]];
    let res1 = this.modelGauss(a_gauss, x);
    let res2 = this.modelPoly(a_poly, x);

    let result = [];

    for(let i = 0; i < res1.length; i++) {
      result.push(res1[i] + res2[i]);
    }

    return result;
  }

  modelGaussAlphaPolyFixed(a, x, alpha, a_poly) {
    let a_gauss = [alpha, a[0], a[1]];
    let res1 = this.modelGauss(a_gauss, x);
    let res2 = this.modelPoly(a_poly, x);

    let result = [];

    for(let i = 0; i < res1.length; i++) {
      result.push(res1[i] + res2[i]);
    }

    return result;
  }

  find_bin_range(range1, range2) {
    let xbin_start = 0, xbin_end = this.bins.length-1;

    for(let i = 0; i < this.bins.length; i++) {
      if (this.bins[i].x0 >= range1) {
        xbin_start = i;
        break;
      }
    }

    for(let i = this.bins.length-1; i >= 0; i--) {
      if (range2 >= this.bins[i].x0) {
        xbin_end = i;
        break;
      }
    }

    return {xbin_start: xbin_start, xbin_end: xbin_end};
  }

  /*
   fitmass(): fit mass distribution 
  */ 
  fitmass() {
    let signal = this.rangeSignal;
    let background = this.rangeBackground;
    let p0;

    let background_start = background[0];
    let background_end = background[1];

    let signal_start = signal[0];
    let signal_end = signal[1];

    let background_binrange = this.find_bin_range(background_start, background_end);
    let signal_binrange = this.find_bin_range(signal_start, signal_end);
    let background_length = background_end - background_start;
    let signal_length = signal_end - signal_start;

    let count = background_binrange.xbin_end - background_binrange.xbin_start;

    p0 = [...this.fit_hint];

    //Step 1: try to fit background
    let chi2_loss_bg = (p) => {
      let res = 0;

      for (let i=0; i < count; i++) {
        let index = i+background_binrange.xbin_start;
        if (index >= signal_binrange.xbin_start && index < signal_binrange.xbin_end) //skip bins inside signal range
          continue;
        let observed = this.bins[index].length;
        let expected = this.modelPoly(p, this.bins[index].x0)[0];
        let diff = observed - expected;
        res += (diff*diff) / Math.max(1, observed);
      }
      return res;
    }

    let p0_poly = [p0[3], p0[4], p0[5]];

    let p1_poly = nelderMead(chi2_loss_bg, p0_poly).x;

    //Step 2: try to fit gaussian
    let chi2_loss_sg = (p) => {
      let alpha = p[0];
      let mu = p[1];
      let sigma = p[2];

      let res = 0;

      if ( Math.abs(sigma) > background_length || mu > background_end || mu < background_start ) {
        for (let i = 0; i < count; i++) {
          res += 1e10;
        }
        return res;
      }

      for (let i=0; i < count; i++) {
        let index = i+background_binrange.xbin_start;
        let observed = this.bins[index].length;
        let expected = this.modelGaussPolyFixed(p, this.bins[index].x0, p1_poly)[0];
        let diff = observed - expected;
        res += (diff*diff) / Math.max(1, observed);
      }
      return res;
    }

    //p0 = [11, signal_start+signal_length/2, signal_length/2, 1,1,1];
    //p1 = conjugateGradient(chi2_loss, p0);

    let p0_gauss = [p0[0], p0[1], p0[2]];
    let p1_gauss = nelderMead(chi2_loss_sg, p0_gauss).x;

    let p1 = [...p1_gauss, ...p1_poly]; 

    // console.log(`Fit hint: ${this.fit_hint}`);
    // console.log(`Fit hint chi2: ${chi2_loss(this.fit_hint)}`); 
    // console.log(`Fit final: ${p1}`);
    // console.log(`Fit final chi2: ${chi2_loss(p1.x)}`);

    return {p: p1, binrange: background_binrange};
  }

  /*
   fitter( data ): compute errors on mean and standart devation
   by ckecking chi2 width curve  
  */ 
  fitterGaus( data ) {
    // var xrange = [1814, 1914];
    // var model = this.modelGaus;

    // function chi2 (x){
    //   var valchi2 = [];
    //   var res = 0;
    //   if ( Math.abs(p[1]) > (xrange[1] - xrange[0]) || p[2] > xrange[1] || p[2] < xrange[0] ) {
    //     for (var i = 0; i<data.length; i++ ) {
    //       valchi2.push(1e10);
    //     }
    //   }
    //   for (i=0; i<data.length; i++) {
    //     var observed = data[i].y;
    //     var expected = <any>model(x, data[i].x);
    //     if (observed >0) var tmp = observed - expected;
    //     valchi2.push( tmp );
    //     res += (valchi2[i] * valchi2[i]) / observed ;
    //   }
    //   return res;
    // }

    // // compute error on mean and standart deviation
    // const p = this.fitD0mass( data )
    // var chi2min = chi2(p);
    // var npoints = 1000;

    // var parinterval_mean = p[2] / (50 * npoints);
    // var parinterval_sigma = p[1] / (50 * npoints);

    // var parmin_mean = p[2] - parinterval_mean * npoints / 2;
    // var parmin_sigma = p[1] - parinterval_sigma * npoints / 2;

    // var parval_mean = [];
    // var parval_sigma = [];

    // var chi2val_mean = [];
    // var chi2val_sigma = [];

    // var params = p;
    // for (var j = 0 ; j < npoints ; j++ ){
    //   parval_mean[j] = parmin_mean + j * parinterval_mean;
    //   params[2] = parval_mean[j];
    //   chi2val_mean[j] = { x: params[2], y: chi2( params ) - chi2min}
    // }

    // var params = this.fitD0mass( data )
    // for (var j = 0 ; j < npoints ; j++ ){
    //   parval_sigma[j] = parmin_sigma + j * parinterval_sigma;
    //   params[1] = parval_sigma[j];
    //   chi2val_sigma[j] = { x: params[1], y: chi2( params ) - chi2min}
    // }


    // // ckecking width curve for mean
    // var minval = 0;
    // var maxval = 0;
    // var k = 0;
    // for(; k<npoints; ++k) {
    //   if(chi2val_mean[k].y <= 1){
    //     minval = chi2val_mean[k].x;
    //     break;
    //   } 
    // }
    // for(; k<npoints; ++k){
    //   if(chi2val_mean[k].y >= 1){
    //     maxval = chi2val_mean[k].x;
    //     break;
    //   } 
    // }

    // // ckecking width curve for standart dev
    // var minval_sigma = 0;
    // var maxval_sigma = 0;
    // var k = 0;
    // for(; k<npoints; ++k) {
    //   if(chi2val_sigma[k].y <= 1){
    //     minval_sigma = chi2val_sigma[k].x;
    //     break;
    //   } 
    // }
    // for(; k<npoints; ++k){
    //   if(chi2val_sigma[k].y >= 1){
    //     maxval_sigma = chi2val_sigma[k].x;
    //     break;
    //   } 
    // }
    // this.fitErrorMean = Math.abs(maxval - minval);
    // this.fitErrorSigma = Math.abs(maxval_sigma - minval_sigma);
  }

  updateFitmass() {
    this.fit_points_bckg = [];
    this.fit_points_full = [];

    let poly_p = [this.fit_current[3], this.fit_current[4], this.fit_current[5]];

    const end_xaxis = this.bins[this.fit_current_binrange.xbin_end - 1].x0;
    const start_xaxis = this.bins[this.fit_current_binrange.xbin_start].x0;

    const interval = (end_xaxis - start_xaxis)/this.fit_points_total;

    for(let x = start_xaxis; x < end_xaxis; x+=interval) {
      let p1 = {x: x, y: this.modelGaussPoly(this.fit_current, x)[0]};
      let p2 = {x: x, y: this.modelPoly(poly_p, x)[0]};
      this.fit_points_full.push(p1);
      this.fit_points_bckg.push(p2);
    }

    let line = d3.line()
      .x( d => {return this.xScale(d.x);} )
      .y( d => {return this.yScale(d.y);} );

    this.signalPath
                .attr("d", line(this.fit_points_full))

    this.backgroundPath
                .attr("d", line(this.fit_points_bckg))
  }

  /*
    drawFitmass(): draw fitted curve 
  */
  drawFitmass(){
    let fit = this.fitmass();

    this.fit_current = fit.p;
    this.fit_current_binrange = fit.binrange;

    this.updateFitmass();
  }

  /*
   generateGaussian(): generate data from a Gaussian model for testing
  */
  generateGaussian( ) {
    var data = [];
    var x, y;
    var point;
    function fNorm (x, mu, sigma, nSignal) {
      var ni1 = 1/(sigma*Math.sqrt(2*Math.PI)); 
      var ni2 = Math.exp(-1*((x-mu)*(x-mu))/(2*(sigma*sigma))); 
        return ( ni1*ni2*nSignal );
    }
    for( var i=0; i<=100; i++){
      x = 1800 + i;
      y = 100 + fNorm (x, 1850, 7, 30000);
      point = { x: x, y: y};
      data.push(point);
    }
    return (data);
  }

  /*
    addLimitesGaus(): add thresholds for min and max signal range
  */
  addLimitesGaus() {
    var margin = {top: 20, right: 20, bottom: 40, left: 60},
        width = 560 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    // var line = this.D0MassChart.append("line")
    //   .attr("x1", d => { return this.xScale(this.maxRangeGaus)})
    //   .attr("x2", d =>{ return this.xScale(this.maxRangeGaus)})
    //   .attr("y1", 0)
    //   .attr("y2", height - 50)
    //   .attr("stroke-width", 1)
    //   .attr("stroke", "black")
    //   .attr("stroke-dasharray", "5")

    // var line = this.D0MassChart.append("line")
    //   .attr("x1", d => { return this.xScale(this.minRangeGaus)})
    //   .attr("x2", d =>{ return this.xScale(this.minRangeGaus)})
    //   .attr("y1", 0)
    //   .attr("y2", height - 50)
    //   .attr("stroke-width", 1)
    //   .attr("stroke", "black")
    //   .attr("stroke-dasharray", "5")
  }

  /*  plotChart( data, minXdomain, maxXdomain, name, iddata, coeffvalues, xLegend , log10)
      plot variable chart
     
      data:               Array[][4] - data read from .json
      minXdomain:         number - minimum x-axis
      maxXdomain:         number - maximum x-axis
      name:               string - name of div allocated for the chart
      iddata:             number - index of line corresponding to variable in data
      coeffvalues:        number - variables PT and TAU need to be multiple by a coefficient
      xLegend:            string - x-axis legend
      log10:              boolean - true if variable is IP
  */
  plotChart( data, minXdomain, maxXdomain, name, xLegend ) {
  //   var margin = {top: 20, right: 20, bottom: 40, left: 60},
  //       width = 560 - margin.left - margin.right,
  //       height = 400 - margin.top - margin.bottom;

  //   this.xScale = d3.scaleLinear<number>()
  //     .rangeRound([0, width - margin.left - 20])
  //     .domain([minXdomain, maxXdomain]);

  //   // append SVG            
  //   var svg = d3.select(name)
  //               .append("svg")
  //               .attr("width", "100%")
  //               .attr("height", "100%")
  //               .attr("viewBox", "0 0 "+width+" "+height);

  //   this.D0Chart = svg.append("g").attr('transform', `translate(${margin.left}, ${margin.top})`);
  //   this.yScale = d3.scaleLog().range([height - margin.left, 0]);
    
  //   if ( name == ".histo_IP") {
  //     this.yScale.domain([.000001, .1]);
  //   } else {
  //     this.yScale.domain([.00001, 1]);
  //   }

  //   // draw y-axis
  //   this.yAxis = this.D0Chart.append('g').attr("class", "yAxix"); 

  //   // x-axis title
  //   this.D0Chart.append("g")
  //     .attr("transform", "translate(0," + (height - margin.bottom -20) + ")")
  //       .call(d3.axisBottom(this.xScale))
  //         .attr("text-anchor", "end")
  //            .append("text")
  //            .attr("transform", `translate(${2*width/3}, ${margin.bottom - 10})` )
  //            .attr("text-anchor", "end")
  //            .attr("stroke", "black")
  //            .text(xLegend);

  //   // y-axis title
  //   this.D0Chart.append("g")
  //     .call(d3.axisLeft(this.yScale))
  //       .append("text")
  //         .attr("transform", "rotate(-90)")
  //         .attr("y", 6)
  //         .attr("dy", "-5.1em")
  //         .attr("text-anchor", "end")
  //         .attr("stroke", "black")
  //         .text("D0 Candidates Fraction");
  
  //  // signal bar
  //  this.D0Chart.selectAll(".bar")
  //     .data(this.bins)
  //     .enter().append("rect")
  //     .attr("class", "bar")
  //     .attr("transform", d => {
  //       if ( d[1]-d[2]>0 ) 
  //         return "translate(" + this.xScale(d[0]) + "," + this.yScale((d[1]-d[2])/data.length) + ")";
  //      })
  //     .attr("x", 1)
  //     .attr("width", d =>  { return this.xScale(d[0]+this.interval) - this.xScale(d[0]) - 1; })
  //     .attr("height", d =>  { 
  //       if ( d[1]-d[2]>0 )
  //         return (height - margin.bottom - 20) - this.yScale((d[1]-d[2])/data.length); }) 
  //     .attr("fill", "#4169E1");

  //  // background bar
  //  this.D0Chart.selectAll(".bar2")
  //     .data(this.bins)
  //     .enter().append("rect")
  //     .attr("class", "bar")
  //     .attr("transform", d => {
  //       if ( d[2]>0 ) 
  //         return "translate(" + this.xScale(d[0]) + "," + this.yScale(d[2]/data.length) + ")";
  //      })
  //     .attr("x", 1)
  //     .attr("width", d =>  { return this.xScale(d[0]+this.interval) - this.xScale(d[0]) - 1; })
  //     .attr("height", d =>  { 
  //       if ( d[2]>0 )
  //         return (height - margin.bottom - 20) - this.yScale(d[2]/data.length); }) 
  //     .attr("fill", "#FF0000")
  //     .attr("fill-opacity","0.5");


  //   // signal legend
  //   var legend_sig = this.D0Chart.append('g')
  //                         .attr('class', 'legend')
  //                         .attr('transform', 'translate(' + (40 + 12) + ', 0)');
  //   legend_sig.append('rect')
  //           .attr('x', 250)
  //           .attr('y', 0)
  //           .attr('width', 12)
  //           .attr('height', 12)
  //           .attr('fill', "#4169E1");
  //   legend_sig.append('text')
  //           .text('Signal')
  //           .attr('x', 265)
  //           .attr('y', 10);

  //   // background legend
  //   var legend_bkg = this.D0Chart.append('g')
  //                         .attr('class', 'legend')
  //                         .attr('transform', 'translate(' + (40 + 12) + ', 0)');
  //   legend_bkg.append('rect')
  //           .attr('x', 250)
  //           .attr('y', 20)
  //           .attr('width', 12)
  //           .attr('height', 12)
  //           .attr("fill", "#FF0000")
  //           .attr("fill-opacity","0.5");
  //   legend_bkg.append('text')
  //           .text('Background')
  //           .attr('x', 265)
  //           .attr('y', 30);
  }

  /*
    saveResultLifetime(): save lifetime value and error, and update scatterplot
      This function is link to 'Save result' button
  */
  saveResultLifetime() {
    // if( this.D0LifetimeChart) {
    //   this.meanLifetime.push( [ this.maxRangeDistIP, this.valLifetime, this.errLifetime ] );
    //   this.fittedD0lifetime();
    // } else {
    //   this.Info("You must plot distribution before.")
    // }
    // console.log(this.meanLifetime);
  }

  // resetExercise(): delete all charts and reinitialize fields
  /*resetExercise() {
    // delete charts
    d3.selectAll("svg").remove();
    d3.selectAll(".infoMath").remove();
    // reset 
    this.D0MassChart = null;
    this.fit = null;
    // reset boolean
    this.displayMass = true;
    this.displayFit = false;
    // reload component
    this.ngOnInit();
  }*/

  /*
   openInstructions(): open instruction notification
  */
  openInstructions() {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '50%',
      data: {  modalTitle: this.titleInstructions, 
               modalText: this.textInstructions,
               addlogo:true}
    });
  }

  titleInstructions = ''//'Welcome to the LHCb masterclass exercise on measuring the lifetime of the D0 meson.'
  textInstructions = ''
  // 'The goal of this exercise is to measure the lifetime of the D0 meson, a fundamental particle'
  // +' made of a charm quark and an up anti-quark. In order to do so, you will first learn how to '
  // +'separate signal D0 mesons from backgrounds. Finally, you will compare your results to the '
  // +'values found by the Particle Data Group (http://pdgLive.lbl.gov).'
  // +'\n\n'
  // +'Step-by-step instructions :'
  // +'\n'
  // +'  1. Plot the D0 mass distribution. The mass of the D0 is a fundamendal variable which '
  // +'separates signal (the peaking structure in the middle) from the flat background.'
  // +'\n'
  // +'  2. Read the results of the fit and use them to determine the signal range. The function '
  // +'being fitted to the signal is a Gaussian, whose width, indicated by the greek letter σ, '
  // +'is related to how far the signal extends from the mean for most probable) value. In '
  // +'particular, an interval of ±1 σ around the mean value contains 68% of the signal, while'
  // +' ±3 σ contains 99.7% of the signal. Use the slider to set the signal range to be ±3 σ '
  // +' around the mean value.'
  // +'\n'
  // +'  3. Plot the variable distributions. You will see three further plots appearing, and in each '
  // +'one the blue points represent the distribution of the signal in that variable while the red '
  // +'points represent the distribution of the background. The plot is logarithmic in the Y axis, '
  // +'and each point represents the fraction of the total signal in that bin. Which regions of '
  // +'each variable contain mostly signal? Which contain mostly background ?'
  // +'\n'
  // +'  4. Fit the lifetime distribution. Save the results of your fit and compare them to the PDG' 
  // +'value. Do they agree ?'
  // +'  5. Repeat step 4 but now varying the upper D0 log(IP) variable range from 1.5 to -2 in '
  // +'steps of 0.2. Do you notice a pattern?'
  // +'Talk to a demonstrator about your results. Does the D0 lifetime with an log(IP) cut of' 
  // +'-1.5 agree better or worse with the PDG than the lifetime with an log(IP) cut of 1.5 ?'

  /*
   principal function
  */ 
  ngOnInit() {
    // to test mass fit
    /*var data = {};
    data = this.generateGaussian();
    this.fitD0mass(data);
    */

   this.zoom_xmin = 0.0;
   this.zoom_xmax = 2.0;
   this.fit_points_bckg = [];
   this.fit_points_full = [];

   this.rangeSignal = [this.zoom_xmin, this.zoom_xmax];
   this.rangeBackground = [this.zoom_xmin, this.zoom_xmax];
  }
}