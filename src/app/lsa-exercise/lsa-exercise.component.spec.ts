import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { D0dataService } from '../services/lsadata.service';
import { HttpClientModule } from '@angular/common/http';
import { ToastrConfig, ToastrModule } from 'ngx-toastr';

import { D0ExerciseComponent } from './lsa-exercise.component';

describe('LSAExerciseComponent', () => {
  let component: D0ExerciseComponent;
  let fixture: ComponentFixture<D0ExerciseComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ D0ExerciseComponent ],
      imports: [  FormsModule, 
                  ReactiveFormsModule,
                  NouisliderModule,
                  HttpClientModule,
                  ToastrModule.forRoot() ],
      providers : [ D0dataService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(D0ExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create d0-exercice component', () => {
    expect(component).toBeTruthy();
  });*/

  /*it('test gauss', () => {
    var data = component.generateGaussian();
    //expect(component.fitD0mass(data)).toEqual([30000, 7, 1850, 100]);
    expect(component.fitD0mass(data)[0]).toBeCloseTo( 30000, -1);
  });*/
  


  //expect(component.fitD0mass(data)[0]).toBeCloseTo( 30000, -1);
});
