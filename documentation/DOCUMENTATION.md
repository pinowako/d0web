# Documentation of LHCb Masterclass web version
## Introduction
LHCB Masterclass web version is a redesign of LHCb masterclass software implemented in C++. You can find the previous version here : https://gitlab.cern.ch/LHCbOutreach/LHCbMasterclass.

The application is split in two exercises:

1.	**Event Display Exercise** aims to identify D0 decay vertices in the vertex detector of the LHCb experiment. 
2.	**D0 lifetime** aims to fit data to extract data from the background and use this signal field to measure the lifetime of the D0 particle.

This web version is build with Angular 8 and mainly uses 2 javascript librairies:

- Three.js for the event display  (https://threejs.org/)
- D3.js to display dynamic data visualisations (https://github.com/d3/d3/wiki)

You can find the documentation of Angular here:https://angular.io/docs

The web application is available here: http://test-lhcb-masterclass.web.cern.ch

## Table of contents
1.	[Getting started](#1-getting-started)
2.	[Overview](#2-overview)
3.	[Data](#3-data)
4.	[Homepage](#4-homepage)
5.	[Event display Exercise](#5-event-display-exercise)
6.	[D0 Lifetime Exercise](#5-d0-lifetime-exercise)


## 1. Getting started
Angular requires Node.js version 10.9.0 or later. Check you version running `node -v` in a terminal/console window. Or, to get `Node.js` go to nodejs.org.

To run the project:
```
git clone https://gitlab.cern.ch/mablanch/mclasswebversion.git
cd mclasswebversion
npm install
ng serve
```
Navigate to `http://localhost:4200/`.

### Building the project
Run `ng build` to build the project. The build artifact will be stored in the `dist/` directory. Use `-prod` flag for a production build.

## 2. Overview 
`src/app` is the application root folder and contains all components.

<p align="center">
	<img src="./overview.png" width="560" height="300" />
</p>

_Event-diplay_ and _D0-exercise_ are the main components: they contain features of both exercises.

_Header Component_ contains the toolbar for top-level title and controls.

_Modal Component_ open dialog box in both exercises to display instructions (see https://material.angular.io/components/dialog/overview)

In Angular application Services focus on data access. Here, `event.service.ts` define `Particle` interface and _getEvent(url)_ method which fetch event data from .json file. `D0data.service.ts` define `D0` interface with properties MM, PT, TAU and MINIP. _getdata(url)_ fetch data from `masterclass_data.json`.

## 3. Data
All data required to display events and build histograms are placed in `mclasswebversion/src/assets` folder. There are two subfolders :

- **'events'** for `.json` files related to events. Format file is `event_x_y.json` with 0≤x≤33 and 0≤y≤29. There are 32 combinations each with 30 events. The code to export the masterclass events to `.json` is here https://gitlab.cern.ch/bcouturi/LHCbMasterclass/blob/data_exporter/src/D0_Lifetime/Exporter.cpp.
- **'img'** for exercise pictures on home page and LHCb logo.

Data use in exercise 2 for display histogramms are built thanks to `masterclass_data.json`.  https://gitlab.cern.ch/bcouturi/LHCbMasterclass/tree/data_exporter/convert_ntuple.


## 4. Homepage
<p align="center">
	<img src="./homepage.png" width="560" height="300" />
</p>
*Homepage screenshot*

### 4.1 Authentification 
_Home Component_ initialises a form and intantiates a Student object with values: name, grade and combination. Thank to _Combination_ service, this object is save and combination value can be send to the event display exercise.

Methods _sendCombi(Student)_ save values and _getCombi()_ makes it possible to recover Student's value from any other component.

### 4.2 List of exercises
Two routerLink define in the `home.component.html` allow navigation to components _Event-diplay_ and _D0-exercise_.

## 5. Event display Exercise
<p align="center">
	<img src="./event-display.png" width="560" height="300" />
</p> 
*Event-display exercise screenshot*

This page is split in 3 parts: 

- event control dashboard
- event display scene with tracks and LHCb detector
- the observation panel.

### 5.1 Event control 

Buttons **Previous** and **Next** are associated to _next()_ and _previous()_ functions wrote in `event-display.component.ts`. They allow to modify current event and to display each one of a same combination. Both buttons reset zoom, clear "Particle information" panel, clear scene deleting tracks and load next/previous event.  

The view fieldset contains 3 slide toggles 
https://material.angular.io/components/slide-toggle/overview.

Each toggle is linked to a function wrote in the typescript file:

- _zoomChange(value: MaSlideToggleChange)_: uses _zoom()_ and _zoomReset()_ function to zoom and center on collision or replace scene and camera at their initial position.
- _hideDetChange(value: MaSlideToggleChange)_: remove or re-load detector gemotry.
- _helpD0Change(value: MaSlideToggleChange)_: _helpFindD0()_ zoom on colision, highlight D0, pi- and K- by increasing their linewidth. _helpFindD0Reset()_ function reset line widths. 

Legend field is built in the `event-display.component.html` file. 

### 5.2 Event display
The event display is built with javascript Three.js

#### 5.2.1 Scene
Scene is render in a canvas on the `event-display.component.html` page.
```html
<canvas #canvas 
	(mousedown)="onMouseDown($event)" 
   		 (mousemove)="onMouseMove($event)"
  	(window:resize)="onWindowResize($event)">		
</canvas>
```
_createScene()_ function implements a scene, a camera, a renderer and controls. _createLight()_ function add one ambient ligth and two directional ligths to the scene.

_startRenderingLoop()_ method create a loop that causes the renderer to draw 
  the scene every time the screen is refreshed.

#### 5.2.2 Tracks
Two functions are needed to display tracks:

- _createEvent(event: Particle[])_ fetches coordinates and particle information (name, E...) from current json, draws spline curves and add these lines to 3D object called _allllines_. 3D object is finally add to the scene.
- _showEvent()_: load json file and put results in a Particle[] structure. The _eventservice_ `event.service.ts`fetches JSON file with a get() method. Then, _Event-diplay_ component injects _eventservice_ and calls _getEvent()_ service method. The subscription callback copies the data fields into Particle[].

##### Mouse event
Mouse can interact with tracks thanks to  
- _onMouseMove( event: MouseEvent )_: change color of the particle hovered and display related information.
- _onMouseDown( event: MouseEvent)_ : two particles consecutivly selected are saved into "My particle" box and mass associeted is computed and display into "Mass" field. This function uses _saveParticle(selected: Particle)_ when two tracks are selected, then uses _calculate()_ for mass calculation.

#### 5.2.3 Detector
_loadDetector()_ uses GLTFLaoder to load and lhcb.glft file (see https://threejs.org/docs/#examples/en/loaders/GLTFLoader). 

### 5.3 The observation panel
#### 5.3.3 Particle information
This panel displays information of current hovered particle (`theparticle`).  

#### 5.3.2 My particle
"My particle" box displays the two particles consecutively selected. `myParticle1` and `myParticle2` are instanciated through the function _saveParticle(selected: Particle)_. Then this function is called when user click on a track with the _onMouseDown(event: MouseEvent)_ function.

#### 5.3.3 Mass and 'Add' button
The _massCalculated_ variable is initialised in _masscalculated()_ function. This function is called when two particles are consecutivly selected. The result is display in "Mass" box.

`Add` button is related to _add()_ function. This function check if mass calculated is a good candidate, push value in a table called `massCalculated` and update histogramm through _updateHisto(this.massSaved)_ method.

#### 5.3.4 Histogramm
Invariant mass histogramm is managed by two methods:

- _drawHisto(data)_ : draw SVG element, add axis and initialise bins.
- _updateHisto( data )_: update y-axis, bars and math information (entries, mean and deviation).
 

## 6. D0 Lifetime Exercise
<p align="center">
	<img src="./d0-exercise.png" width="560" height="300" />
</p>
*D0 lifetime exercise screenshot*

The right side pannel allow to display histogramms and change range variables. 

### 6.1 Plot D0 mass
`masterclass_data.jon` file is load in the function `showdata()`. This method fetchs data and puts them into D0[] variable called `D0data`. 

_initBins( data, index, minBin, maxBin )_ initialises 110 bins between minBin and maxBin.

Then `this.plotD0Mass(data, divName)` is called to build and display D0 mass distribution in ```<div class="histo_MM"></div>```.

`showdata()` is called when user click on button 'Plot D0 mass'.

### 6.2 Fit mass distribution

`fitMassDist( data )` function fit mass distribution with a Gaussian curve defined by `modelGaus(a, x)`. Then chi squared is computed and minimise with Nelder Mead method (see https://github.com/benfred/fmin)

`fitterGaus( data )` compute errors on mean and standart devation
by ckecking chi2 width curve.

Finally `drawFitD0mass( data )` display fitted curve and information such as mean and standart deviation with their error.

### 6.3 Plot distributions
`Plot Distribution` button is linked to onSubmitDist() method. It initialises form values, checks if background substraction is not null and call `plotDistribution()` function.

`plotDistribution()` function displays and updates 5 charts according to filtered data:
- mass distribution and fit
- PT, IP and TAU distribution
- TAU signal distribution and fitted lifetime.

_addLimitesGaus()_ adds thresholds for min and max signal range input.

`plotChart( data, minXdomain, maxXdomain, name, iddata, coeffvalues, xLegend , log10 )` build and display IP, PT and TAU histogram.s Its parameters are:

- data:               Array[][4] - data read from .json
- minXdomain:         number - minimum x-axis
- maxXdomain:         number - maximum x-axis
- name:               string - name of div allocated for the chart
- iddata:             number - index of line corresponding to variable in data
- coeffvalues:        number - variables PT and TAU need to be multiple by a coefficient
- xLegend:            string - x-axis legend
- log10:              boolean - true if variable is IP


### 6.4 Exponential fit
_plotFitExpo()_, on a same manner as the previous function, build and display TAU histograms but only with signal values.

_saveResultLifetime()_ is linked to `Save result` button. This function creates an array formed of triplet : max IP cut, fit result and error. 

Finally, _fittedD0lifetime()_ create a scatterplot formed of the saved values.




